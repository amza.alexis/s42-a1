const txtFirstName = document.querySelector("#txt-first-name")
const txtLastName = document.querySelector("#txt-last-name")
const spanFullName = document.querySelector("#span-full-name")


function combine() {
	let text1 = txtFirstName.value
	let text2 = txtLastName.value
	spanFullName.innerHTML = `${text1} ${text2}`
}

txtFirstName.addEventListener('input', combine)
txtLastName.addEventListener('input', combine)